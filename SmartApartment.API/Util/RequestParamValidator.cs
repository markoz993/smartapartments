﻿using System;

namespace SmartApartment.API.Util
{
    public class RequestParamValidator
    {
        public Func<bool> Rule { get; }

        public string Message { get; }

        public RequestParamValidator(Func<bool> rule, string message)
        {
            Rule = rule;
            Message = message;
        }

        public static RequestParamValidator ManagementIdValidator(int id) =>
            new RequestParamValidator(
                IdInvalidRule(id), "Management id is not valid.");

        public static RequestParamValidator PropertyIdValidator(int id) =>
            new RequestParamValidator(
                IdInvalidRule(id), "Property id is not valid.");
        
        private static Func<bool> IdInvalidRule(int id)
        {
            return () => id <= 0;
        }
    }
}
