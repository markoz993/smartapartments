﻿using SmartApartment.Application.Commands.Management;
using SmartApartment.API.Util;

using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

namespace SmartApartment.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Products")]
    [Authorize(Roles = "admin,developer")]
    public class ManagementController : BaseApiController
    {
        public ManagementController(ILogger<ManagementController> logger) : base(logger) { }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> List() =>
            await ProcessRequest(
                request: () => Mediator.Send(new ListManagementsRequest()));

        [HttpGet]
        public async Task<IActionResult> Get(int id) =>
            await ProcessRequest(
                request: () => Mediator.Send(new ProductDetailsRequest { ManagementID = id }),
                paramValidators: RequestParamValidator.ManagementIdValidator(id));
    }
}