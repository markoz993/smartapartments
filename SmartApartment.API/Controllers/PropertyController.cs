﻿using SmartApartment.Application.Commands.Property;
using SmartApartment.API.Util;

using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

namespace SmartApartment.API.Controllers
{
    [Route("api/Brands")]
    [Produces("application/json")]
    [Authorize(Roles = "admin")]
    public class PropertyController : BaseApiController
    {
        public PropertyController(ILogger<PropertyController> logger) : base(logger) { }

        [HttpGet]
        public async Task<IActionResult> List() =>
            await ProcessRequest(request: () => Mediator.Send(new ListPropertiesRequest()));

        [HttpGet]
        public async Task<IActionResult> Get(int id) =>
            await ProcessRequest(
                request: () => Mediator.Send(new PropertyDetailsRequest() { PropertyID = id }),
                paramValidators: RequestParamValidator.PropertyIdValidator(id));
    }
}