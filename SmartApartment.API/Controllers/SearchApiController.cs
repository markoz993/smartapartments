﻿using SmartApartment.Application.Commands.SearchAPI;
using SmartApartment.Application.Commands.Search;

using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

using Nest;

namespace SmartApartment.API.Controllers
{
    public class SearchApiController : BaseApiController
    {
        private readonly IElasticClient _elasticClient;

        public SearchApiController(IElasticClient elasticClient, ILogger<SearchApiController> logger) : base(logger)
        {
            _elasticClient = elasticClient;
        }

        [Route("/searchApi")]
        [Authorize(Roles = "admin,developer")]
        public async Task<IActionResult> SearchApi(string searchWord, string market, int currentPage = 1, int resultsPerPage = 10) =>
            await ProcessRequest(request: () => Mediator.Send(new SearchApiDataRequest
            { SearchWord = searchWord, Market = market, CurrentPage = currentPage, ResultsPerPage = resultsPerPage }));

        [Route("/search")]
        [Authorize(Roles = "admin,developer")]
        public async Task<IActionResult> Search(string searchWord, string market) =>
            await ProcessRequest(request: () => Mediator.Send(new SearchDataRequest
            { SearchWord = searchWord, Market = market }));
    }
}