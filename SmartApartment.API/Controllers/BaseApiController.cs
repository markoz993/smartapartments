﻿using SmartApartment.API.Util;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;
using System.Linq;
using System;

using MediatR;

namespace SmartApartment.API.Controllers
{
    public class BaseApiController : Controller
    {
        private IMediator _mediator;
        protected IMediator Mediator
        {
            get
            {
                return _mediator ?? (_mediator = HttpContext.RequestServices.GetService<IMediator>());
            }
        }

        private readonly ILogger<BaseApiController> _logger;
        public BaseApiController(ILogger<BaseApiController> logger)
        {
            this._logger = logger;
        }

        protected async Task<IActionResult> ProcessCommand(Func<Task> command, params RequestParamValidator[] paramValidators)
        {
            var invalidParams = paramValidators?.Where(pv => pv.Rule());

            if (invalidParams.Any())
            {
                var message = string.Join(
                    ". ", invalidParams.Select(s => s.Message));
                return BadRequest(message);
            }

            try
            {
                await command();
                return Ok(Response);
            }

            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                return StatusCode(500);
            }
        }

        protected async Task<IActionResult> ProcessRequest<T>(Func<Task<T>> request, params RequestParamValidator[] paramValidators)
        {
            var invalidParams = paramValidators?
                .Where(pv => pv.Rule());

            if (invalidParams.Any())
            {
                var message = string.Join(
                    ". ", invalidParams.Select(s => s.Message));
                return BadRequest(message);
            }

            try
            {
                var response = await request();

                if (response == null)
                {
                    return NotFound();
                }

                return Ok(response);
            }

            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                return StatusCode(500);
            }
        }
    }
}