﻿namespace SmartApartment.API.Logging
{
    public interface ILogger
    {
        void LogMessage(string message);
    }
}
