﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore;

using Serilog.Events;
using Serilog;

namespace SmartApartment.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                  .Enrich.FromLogContext()
                  .MinimumLevel.Debug()
                  .WriteTo.ColoredConsole(
                      LogEventLevel.Verbose,
                      "{NewLine} [{Level}] {Timestamp:HH:mm:ss} ({CorrelationToken}) {Message}{NewLine}{Exception}")
                      .CreateLogger();

            try
            {
                CreateWebHostBuilder(args).Build().Run();
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseSerilog()
                .UseStartup<Startup>();
    }
}
