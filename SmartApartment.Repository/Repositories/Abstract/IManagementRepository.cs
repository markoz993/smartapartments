﻿using SmartApartment.Domain.Entities;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartApartment.Repository.Repositories.Abstract
{
    public interface IManagementRepository
    {
        Task<IEnumerable<Management>> GetAllAsync();

        Task<IEnumerable<Management>> GetManagementsWithGivenName(string name);
        Task<IEnumerable<Management>> GetManagementsByMarket(string market);
        Task<IEnumerable<Management>> GetManagementsByState(string state);

        Task<Management> GetByIDAsync(int managementID);

        Task AddAsync(Management management);
        Task EditAsync(Management management);
        Task DeleteAsync(Management management);
    }
}
