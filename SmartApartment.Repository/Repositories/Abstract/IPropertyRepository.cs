﻿using SmartApartment.Domain.Entities;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartApartment.Repository.Repositories.Abstract
{
    public interface IPropertyRepository
    {
        Task<IEnumerable<Property>> GetAllAsync();

        Task<IEnumerable<Property>> GetPropertiesWithGivenName(string name);
        Task<IEnumerable<Property>> GetPropertiesWithGivenFormerName(string name);
        Task<IEnumerable<Property>> GetPropertiesWithGivenStreetAddress(string street);
        Task<IEnumerable<Property>> GetPropertiesWithGivenCity(string city);
        Task<IEnumerable<Property>> GetPropertiesWithGivenMarket(string market);
        Task<IEnumerable<Property>> GetPropertiesWithGivenState(string state);

        Task<Property> GetByIDAsync(int brandID);

        Task AddAsync(Property brand);
        Task EditAsync(Property brand);
        Task DeleteAsync(Property brand);
    }
}
