﻿using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace SmartApartment.Repository.Repositories.Abstract
{
    public interface IEntityBaseRepository<T> where T : class
    {
        IQueryable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties);
        IQueryable<T> GetAll();

        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        IQueryable<T> FindByIncluding(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);

        void Add(T entity);
        void Delete(T entity);
        void Edit(T entity);

        Task SaveAsync();
    }
}
