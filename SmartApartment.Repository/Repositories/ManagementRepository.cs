﻿using SmartApartment.Repository.Repositories.Abstract;
using SmartApartment.Repository.Infrastructure;
using SmartApartment.Domain.Entities;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartApartment.Repository.Repositories
{
    public class ManagementRepository : EntityBaseRepository<Management>, IManagementRepository
    {
        public ManagementRepository(IDbContextFactory factory) : base(factory) { }

        public async Task<IEnumerable<Management>> GetAllAsync() =>
            await GetAll().ToListAsync();

        public async Task<IEnumerable<Management>> GetManagementsWithGivenName(string name) =>
            await FindBy(p => p.Name.ToLower().Contains(name.ToLower())).ToListAsync();

        public async Task<IEnumerable<Management>> GetManagementsByMarket(string market) =>
            await FindBy(m => m.Market.ToLower().Contains(market.ToLower())).ToListAsync();

        public async Task<IEnumerable<Management>> GetManagementsByState(string state) =>
            await FindBy(m => m.State.ToLower().Contains(state.ToLower())).ToListAsync();

        public async Task<Management> GetByIDAsync(int managementID) =>
            await FindByIncluding(p => p.ID == managementID).FirstOrDefaultAsync();

        public async Task AddAsync(Management product)
        {
            Add(product);
            await SaveAsync();
        }
        public async Task EditAsync(Management product)
        {
            Edit(product);
            await SaveAsync();
        }
        public async Task DeleteAsync(Management product)
        {
            Delete(product);
            await SaveAsync();
        }
    }
}
