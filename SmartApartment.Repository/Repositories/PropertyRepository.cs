﻿using SmartApartment.Repository.Repositories.Abstract;
using SmartApartment.Repository.Infrastructure;
using SmartApartment.Domain.Entities;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartApartment.Repository.Repositories
{
    public class PropertyRepository : EntityBaseRepository<Property>, IPropertyRepository
    {
        public PropertyRepository(IDbContextFactory factory) : base(factory) { }

        public async Task<IEnumerable<Property>> GetAllAsync() =>
            await GetAll().ToListAsync();

        public async Task<IEnumerable<Property>> GetPropertiesWithGivenName(string name) =>
            await FindBy(b => b.Name.ToLower().Contains(name.ToLower())).ToListAsync();

        public async Task<IEnumerable<Property>> GetPropertiesWithGivenFormerName(string name) =>
            await FindBy(b => b.FormerName.ToLower().Contains(name.ToLower())).ToListAsync();

        public async Task<IEnumerable<Property>> GetPropertiesWithGivenStreetAddress(string street) =>
            await FindBy(b => b.StreetAddress.ToLower().Contains(street.ToLower())).ToListAsync();

        public async Task<IEnumerable<Property>> GetPropertiesWithGivenCity(string city) =>
            await FindBy(b => b.City.ToLower().Contains(city.ToLower())).ToListAsync();

        public async Task<IEnumerable<Property>> GetPropertiesWithGivenMarket(string market) =>
            await FindBy(b => b.Market.ToLower().Contains(market.ToLower())).ToListAsync();

        public async Task<IEnumerable<Property>> GetPropertiesWithGivenState(string state) =>
            await FindBy(b => b.State.ToLower().Contains(state.ToLower())).ToListAsync();

        public async Task<Property> GetByIDAsync(int exerciseID) =>
            await FindByIncluding(b => b.ID == exerciseID).FirstOrDefaultAsync();

        public async Task AddAsync(Property brand)
        {
            Add(brand);
            await SaveAsync();
        }
        public async Task EditAsync(Property brand)
        {
            Edit(brand);
            await SaveAsync();
        }
        public async Task DeleteAsync(Property brand)
        {
            Delete(brand);
            await SaveAsync();
        }
    }
}
