﻿using SmartApartment.Repository.Repositories.Abstract;
using SmartApartment.Repository.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace SmartApartment.Repository.Repositories
{
    public class EntityBaseRepository<T> : IEntityBaseRepository<T> where T : class
    {
        private readonly IDbContextFactory _dbContextFactory;

        public EntityBaseRepository(IDbContextFactory factory)
        {
            _dbContextFactory = factory;
        }

        protected SmartApartmentsDbContext DbContext => _dbContextFactory?.DbContext;

        public IQueryable<T> GetAll() =>
            DbContext.Set<T>();

        public IQueryable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            var result = DbContext.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                result.Include(includeProperty);
            }
            return result;
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate) =>
            DbContext.Set<T>().Where(predicate);

        public IQueryable<T> FindByIncluding(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            var result = DbContext.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                result.Include(includeProperty);
            }
            return result.Where(predicate);
        }

        public void Add(T entity)
        {
            DbContext.Set<T>().Add(entity);
            DbContext.SaveChangesAsync();
        }

        public void Edit(T entity)
        {
            DbContext.Set<T>().Update(entity);
            DbContext.SaveChangesAsync();
        }

        public void Delete(T entity)
        {
            DbContext.Set<T>().Remove(entity);
            DbContext.SaveChangesAsync();
        }

        public async Task SaveAsync() =>
            await DbContext.SaveChangesAsync();
    }
}
