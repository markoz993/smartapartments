﻿using SmartApartment.Repository.Configuration;
using SmartApartment.Domain.Entities;

using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace SmartApartment.Repository.Infrastructure
{
    public class SmartApartmentsDbContext : DbContext
    {
        private const string ConnectionStringName = "SmartApartmentsApplication";
        private readonly IConfiguration _configuration;

        public SmartApartmentsDbContext(DbContextOptions<SmartApartmentsDbContext> options, IConfiguration configuration) : base(options)
        {
            _configuration = configuration;
        }

        #region Entity Sets
        public DbSet<Management> ManagementSet { get; set; }
        public DbSet<Domain.Entities.Property> PropertySet { get; set; }
        #endregion

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString(ConnectionStringName));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (IMutableEntityType entityType in modelBuilder.Model.GetEntityTypes())
            {
                entityType.Relational().TableName = entityType.DisplayName();
            }

            modelBuilder.ApplyConfiguration(new ManagementConfiguration());
            modelBuilder.ApplyConfiguration(new PropertyConfiguration());
        }
    }
}
