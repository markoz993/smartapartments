﻿namespace SmartApartment.Repository.Infrastructure
{
    public interface IDbContextFactory
    {
        SmartApartmentsDbContext DbContext { get; }
    }
}
