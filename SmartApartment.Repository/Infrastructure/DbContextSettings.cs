﻿namespace SmartApartment.Repository.Infrastructure
{
    public class DbContextSettings
    {
        /// <summary>
        /// Connection string from appsettings.json file
        /// </summary>
        public string ConnectionString { get; set; }
    }
}
