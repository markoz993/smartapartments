﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;

namespace SmartApartment.Repository.Infrastructure
{
    public class DbContextFactory : IDesignTimeDbContextFactory<SmartApartmentsDbContext>, IDbContextFactory, IDisposable
    {
        public SmartApartmentsDbContext DbContext { get; private set; }

        public DbContextFactory(IOptions<DbContextSettings> settings)
        {
            var options = new DbContextOptionsBuilder().UseSqlServer(settings.Value.ConnectionString).Options as DbContextOptions<SmartApartmentsDbContext>;
            DbContext = new SmartApartmentsDbContext(options, GetConfiguration());
        }

        public SmartApartmentsDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<SmartApartmentsDbContext>();
            return new SmartApartmentsDbContext(builder.Options, GetConfiguration());
        }

        private IConfigurationRoot GetConfiguration()
        {
            string basePath = AppContext.BaseDirectory;
            IConfigurationRoot configuration = new ConfigurationBuilder()
             .Build();

            return configuration;
        }

        ~DbContextFactory()
        {
            Dispose();
        }
        public void Dispose()
        {
            DbContext?.Dispose();
        }
    }
}
