﻿using SmartApartment.Domain.Entities;

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace SmartApartment.Repository.Configuration
{
    public class ManagementConfiguration : IEntityTypeConfiguration<Management>
    {
        public void Configure(EntityTypeBuilder<Management> builder)
        {
            builder.HasKey(m => m.ID).HasName("mgmtID");
            builder.Property(m => m.Name).HasColumnName("name");
            builder.Property(m => m.Market).HasColumnName("market");
            builder.Property(m => m.State).HasColumnName("state");
        }
    }
}
