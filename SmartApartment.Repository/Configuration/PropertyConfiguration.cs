﻿using SmartApartment.Domain.Entities;

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace SmartApartment.Repository.Configuration
{
    public class PropertyConfiguration : IEntityTypeConfiguration<Property>
    {
        public void Configure(EntityTypeBuilder<Property> builder)
        {
            builder.HasKey(p => p.ID).HasName("propertyID");
            builder.Property(p => p.Name).HasColumnName("name");
            builder.Property(p => p.FormerName).HasColumnName("formerName");
            builder.Property(p => p.StreetAddress).HasColumnName("streetAddress");
            builder.Property(p => p.City).HasColumnName("city");
            builder.Property(p => p.Market).HasColumnName("market");
            builder.Property(p => p.State).HasColumnName("state");
            builder.Property(p => p.Lat).HasColumnName("lat");
            builder.Property(p => p.Lng).HasColumnName("lng");
        }
    }
}
