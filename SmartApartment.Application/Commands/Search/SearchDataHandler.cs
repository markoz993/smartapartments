﻿using SmartApartment.Service.ViewModels.Search;
using SmartApartment.Service.Abstract;

using System.Threading.Tasks;
using System.Threading;

using MediatR;

namespace SmartApartment.Application.Commands.Search
{
    public class SearchDataHandler : IRequestHandler<SearchDataRequest, SearchViewModel>
    {
        private readonly ISearchService _searchService;

        public SearchDataHandler(ISearchService searchS)
        {
            _searchService = searchS;
        }

        public async Task<SearchViewModel> Handle(SearchDataRequest request, CancellationToken cancellationToken) =>
            await _searchService.GetSearchResults(request.SearchWord, request.Market);
    }
}
