﻿using SmartApartment.Service.ViewModels.Search;

using MediatR;

namespace SmartApartment.Application.Commands.Search
{
    public class SearchDataRequest : IRequest<SearchViewModel>
    {
        public string SearchWord { get; set; }
        public string Market { get; set; }
    }
}
