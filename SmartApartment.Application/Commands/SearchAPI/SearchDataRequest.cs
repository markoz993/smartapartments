﻿using SmartApartment.Service.ViewModels.SearchApi;

using MediatR;

namespace SmartApartment.Application.Commands.SearchAPI
{
    public class SearchApiDataRequest : IRequest<SearchApiDataViewModel>
    {
        public string SearchWord { get; set; }
        public string Market { get; set; }

        public int CurrentPage { get; set; }
        public int ResultsPerPage { get; set; }
    }
}
