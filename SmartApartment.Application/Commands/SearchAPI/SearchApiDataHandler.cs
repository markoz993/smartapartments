﻿using SmartApartment.Service.ViewModels.SearchApi;
using SmartApartment.Service.Abstract;

using System.Threading.Tasks;
using System.Threading;

using MediatR;

namespace SmartApartment.Application.Commands.SearchAPI
{
    public class SearchApiDataHandler : IRequestHandler<SearchApiDataRequest, SearchApiDataViewModel>
    {
        private readonly ISearchApiService _searchService;

        public SearchApiDataHandler(ISearchApiService searchS)
        {
            _searchService = searchS;
        }

        public async Task<SearchApiDataViewModel> Handle(SearchApiDataRequest request, CancellationToken cancellationToken) =>
            await _searchService.GetSearchResults(request.SearchWord, request.Market, request.CurrentPage, request.ResultsPerPage);
    }
}
