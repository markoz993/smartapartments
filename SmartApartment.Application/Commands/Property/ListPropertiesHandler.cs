﻿using SmartApartment.Service.ViewModels.Property;
using SmartApartment.Service.Abstract;

using System.Threading.Tasks;
using System.Threading;

using MediatR;

namespace SmartApartment.Application.Commands.Property
{
    public class ListPropertiesHandler : IRequestHandler<ListPropertiesRequest, ListPropertiesViewModel>
    {
        private readonly IPropertyService _propertyService;

        public ListPropertiesHandler(IPropertyService propertyS)
        {
            _propertyService = propertyS;
        }

        public async Task<ListPropertiesViewModel> Handle(ListPropertiesRequest request, CancellationToken cancellationToken) =>
            await _propertyService.GetPropertiesList();
    }
}
