﻿using SmartApartment.Service.ViewModels.Property;
using SmartApartment.Service.Abstract;

using System.Threading.Tasks;
using System.Threading;

using MediatR;

namespace SmartApartment.Application.Commands.Property
{
    public class PropertyDetailsHandler : IRequestHandler<PropertyDetailsRequest, PropertyDetailsViewModel>
    {
        private readonly IPropertyService _propertyService;

        public PropertyDetailsHandler(IPropertyService propertyS)
        {
            _propertyService = propertyS;
        }

        public async Task<PropertyDetailsViewModel> Handle(PropertyDetailsRequest request, CancellationToken cancellationToken) =>
            await _propertyService.GetPropertyDetails(request.PropertyID);
    }
}
