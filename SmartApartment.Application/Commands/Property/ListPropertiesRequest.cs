﻿using SmartApartment.Service.ViewModels.Property;

using MediatR;

namespace SmartApartment.Application.Commands.Property
{
    public class ListPropertiesRequest : IRequest<ListPropertiesViewModel>
    {
    }
}
