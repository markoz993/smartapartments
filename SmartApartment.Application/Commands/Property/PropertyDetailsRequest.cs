﻿using SmartApartment.Service.ViewModels.Property;

using MediatR;

namespace SmartApartment.Application.Commands.Property
{
    public class PropertyDetailsRequest : IRequest<PropertyDetailsViewModel>
    {
        public int PropertyID { get; set; }
    }
}
