﻿using SmartApartment.Service.ViewModels.Management;
using SmartApartment.Service.Abstract;

using System.Threading.Tasks;
using System.Threading;

using MediatR;

namespace SmartApartment.Application.Commands.Management
{
    public class ManagementDetailsHandler : IRequestHandler<ProductDetailsRequest, ManagementDetailsViewModel>
    {
        private readonly IManagementService _managementService;

        public ManagementDetailsHandler(IManagementService managementS)
        {
            _managementService = managementS;
        }

        public async Task<ManagementDetailsViewModel> Handle(ProductDetailsRequest request, CancellationToken cancellationToken) =>
            await _managementService.GetManagementsDetails(request.ManagementID);
    }
}
