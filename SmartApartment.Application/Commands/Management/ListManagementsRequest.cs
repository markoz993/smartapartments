﻿using SmartApartment.Service.ViewModels.Management;

using MediatR;

namespace SmartApartment.Application.Commands.Management
{
    public class ListManagementsRequest : IRequest<ListManagementsViewModel>
    {
    }
}
