﻿using SmartApartment.Service.ViewModels.Management;
using SmartApartment.Service.Abstract;

using System.Threading.Tasks;
using System.Threading;

using MediatR;

namespace SmartApartment.Application.Commands.Management
{
    public class ListManagementsHandler : IRequestHandler<ListManagementsRequest, ListManagementsViewModel>
    {
        private readonly IManagementService _managementService;

        public ListManagementsHandler(IManagementService managementS)
        {
            _managementService = managementS;
        }
        public async Task<ListManagementsViewModel> Handle(ListManagementsRequest request, CancellationToken cancellationToken) =>
            await _managementService.GetManagementsList();
    }
}
