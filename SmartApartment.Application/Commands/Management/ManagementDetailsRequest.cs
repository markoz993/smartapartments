﻿using SmartApartment.Service.ViewModels.Management;

using MediatR;

namespace SmartApartment.Application.Commands.Management
{
    public class ProductDetailsRequest : IRequest<ManagementDetailsViewModel>
    {
        public int ManagementID { get; set; }
    }
}
