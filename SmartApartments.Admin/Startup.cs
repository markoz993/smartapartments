﻿using SmartApartment.Repository.Repositories.Abstract;
using SmartApartment.Repository.Infrastructure;
using SmartApartment.Repository.Repositories;
using SmartApartment.Service.SearchEngine;
using SmartApartments.Admin.Extensions;
using SmartApartment.Service.Abstract;
using SmartApartment.Service.Services;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

namespace SmartApartments.Admin
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddTransient<ISearchEngine, SearchEngine>();
            services.AddTransient<IPropertySearchEngine, PropertySearchEngine>();
            services.AddTransient<IManagementSearchEngine, ManagementSearchEngine>();

            services.AddTransient<IManagementService, ManagementService>();
            services.AddTransient<IPropertyService, PropertyService>();
            services.AddTransient<ISearchApiService, SearchApiService>();
            services.AddTransient<ISearchService, SearchService>();

            services.AddTransient<IManagementRepository, ManagementRepository>();
            services.AddTransient<IPropertyRepository, PropertyRepository>();

            services.AddSingleton<IDbContextFactory, DbContextFactory>();

            services.AddElasticsearch(Configuration);
            services.AddAuth0(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles();

            app.UseAuthentication();
            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
