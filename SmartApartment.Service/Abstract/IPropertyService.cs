﻿using SmartApartment.Service.ViewModels.Property;

using System.Threading.Tasks;

namespace SmartApartment.Service.Abstract
{
    public interface IPropertyService
    {
        Task<ListPropertiesViewModel> GetPropertiesList();

        Task<PropertyDetailsViewModel> GetPropertyDetails(int id);
    }
}
