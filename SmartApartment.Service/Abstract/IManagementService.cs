﻿using SmartApartment.Service.ViewModels.Management;

using System.Threading.Tasks;

namespace SmartApartment.Service.Abstract
{
    public interface IManagementService
    {
        Task<ListManagementsViewModel> GetManagementsList();

        Task<ManagementDetailsViewModel> GetManagementsDetails(int id);
    }
}
