﻿using SmartApartment.Service.ViewModels.Search;

using System.Threading.Tasks;

namespace SmartApartment.Service.Abstract
{
    public interface ISearchService
    {
        Task<SearchViewModel> GetSearchResults(string searchWord, string market);
    }
}
