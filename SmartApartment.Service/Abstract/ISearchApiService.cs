﻿using SmartApartment.Service.ViewModels.SearchApi;

using System.Threading.Tasks;

namespace SmartApartment.Service.Abstract
{
    public interface ISearchApiService
    {
        Task<SearchApiDataViewModel> GetSearchResults(string searchWord, string market, int currentPage, int resultsPerPage);
    }
}
