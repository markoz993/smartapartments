﻿namespace SmartApartment.Service.Util
{
    public static class Constants
    {
        public const string SearchEngine = "SearchEngine";

        public const string PropertyBoosts = "SearchEngine:PropertyBoosts";
        public const string PropertyName = "SearchEngine:PropertyBoosts:Name";
        public const string PropertyFormerName = "SearchEngine:PropertyBoosts:FormerName";
        public const string PropertyStreetAddress = "SearchEngine:PropertyBoosts:StreetAddress";
        public const string PropertyState = "SearchEngine:PropertyBoosts:State";
        public const string PropertyCity = "SearchEngine:PropertyBoosts:City";

        public const string ManagementBoosts = "SearchEngine:ManagementBoosts";
        public const string ManagementName = "SearchEngine:ManagementBoosts:Name";
        public const string ManagementState = "SearchEngine:ManagementBoosts:State";
    }
}
