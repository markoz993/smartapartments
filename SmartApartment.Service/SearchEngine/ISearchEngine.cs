﻿using SmartApartment.Domain.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartApartment.Service.SearchEngine
{
    public interface ISearchEngine
    {
        Task<List<PropertyDTO>> FilterProperties(List<PropertyDTO> properties, string searchWord);
        Task<List<ManagementDTO>> FilterManagements(List<ManagementDTO> managements, string searchWord);
    }
}
