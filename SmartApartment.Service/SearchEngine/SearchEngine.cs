﻿using SmartApartment.Domain.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartApartment.Service.SearchEngine
{
    public class SearchEngine : ISearchEngine
    {
        private readonly IPropertySearchEngine _propSearchEngine;
        private readonly IManagementSearchEngine _managementSearchEngine;

        public SearchEngine(
            IPropertySearchEngine propSE, 
            IManagementSearchEngine manSE)
        {
            _propSearchEngine = propSE;
            _managementSearchEngine = manSE;
        }

        public async Task<List<PropertyDTO>> FilterProperties(List<PropertyDTO> properties, string searchWord)
        {
            if (!string.IsNullOrWhiteSpace(searchWord))
                properties = await _propSearchEngine.GetBestProperties(properties, searchWord);
            return properties;
        }

        public async Task<List<ManagementDTO>> FilterManagements(List<ManagementDTO> managements, string searchWord)
        {
            if (!string.IsNullOrWhiteSpace(searchWord))
                managements = await _managementSearchEngine.GetBestManagements(managements, searchWord);
            return managements;
        }
    }
}
