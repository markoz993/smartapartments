﻿using SmartApartment.Domain.Models;
using SmartApartment.Service.Util;

using Microsoft.Extensions.Configuration;

using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace SmartApartment.Service.SearchEngine
{
    public class ManagementSearchEngine : IManagementSearchEngine
    {
        private readonly IConfiguration _configuration;

        public ManagementSearchEngine(IConfiguration conf)
        {
            _configuration = conf;
        }

        public async Task<List<ManagementDTO>> GetBestManagements(List<ManagementDTO> managements, string searchWord)
        {
            var result = new List<ManagementDTO>();
            var dictionary = new Dictionary<float, ManagementDTO>();
            foreach (ManagementDTO management in managements)
                await Task.Run(() => dictionary.Add(CalculateHowAccurateIsProperty(management, searchWord), management));

            dictionary = (Dictionary<float, ManagementDTO>)dictionary.OrderBy(d => d.Key).Take(10);
            foreach (var keyValuePair in dictionary)
                await Task.Run(() => result.Add(keyValuePair.Value));
            return result;
        }

        private float CalculateHowAccurateIsProperty(ManagementDTO property, string searchWord)
        {
            float result = CalculateNameAccuracy(property.Name, searchWord, _configuration.GetSection(Constants.ManagementName).Value);
            result += CalculateNameAccuracy(property.State, searchWord, _configuration.GetSection(Constants.ManagementState).Value);
            return result;
        }

        private float CalculateNameAccuracy(string name, string searchWords, string boostStr)
        {
            if (!float.TryParse(boostStr, out float boost))
                throw new Exception("Parsing boost for calculating name accuracy has failed.");
            float result = 0;

            string[] searchWordArray = searchWords.Split(" ", StringSplitOptions.RemoveEmptyEntries);
            foreach (string searchWord in searchWordArray)
            {
                if (name.ToLower().Contains(searchWord.ToLower()))
                {
                    result += searchWord.Length * boost;
                }
            }
            return result;
        }
    }
}
