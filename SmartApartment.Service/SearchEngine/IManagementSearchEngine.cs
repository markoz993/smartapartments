﻿using SmartApartment.Domain.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartApartment.Service.SearchEngine
{
    public interface IManagementSearchEngine
    {
        Task<List<ManagementDTO>> GetBestManagements(List<ManagementDTO> managements, string searchWord);
    }
}
