﻿using SmartApartment.Domain.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartApartment.Service.SearchEngine
{
    public interface IPropertySearchEngine
    {
        Task<List<PropertyDTO>> GetBestProperties(List<PropertyDTO> properties, string searchWord);
    }
}
