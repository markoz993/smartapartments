﻿using SmartApartment.Domain.Models;
using SmartApartment.Service.Util;

using Microsoft.Extensions.Configuration;

using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace SmartApartment.Service.SearchEngine
{
    public class PropertySearchEngine : IPropertySearchEngine
    {
        private readonly IConfiguration _configuration;

        public PropertySearchEngine(IConfiguration conf)
        {
            _configuration = conf;
        }

        public async Task<List<PropertyDTO>> GetBestProperties(List<PropertyDTO> properties, string searchWord)
        {
            var result = new List<PropertyDTO>();
            var dictionary = new Dictionary<float, PropertyDTO>();
            foreach (PropertyDTO property in properties)
                await Task.Run(() => dictionary.Add(CalculateHowAccurateIsProperty(property, searchWord), property));

            dictionary = (Dictionary<float, PropertyDTO>)dictionary.OrderBy(d => d.Key).Take(10);
            foreach (var keyValuePair in dictionary)
                await Task.Run(() => result.Add(keyValuePair.Value));
            return result;
        }

        private float CalculateHowAccurateIsProperty(PropertyDTO property, string searchWord)
        {
            float result = CalculateNameAccuracy(property.Name, searchWord, _configuration.GetSection(Constants.PropertyName).Value);
            result += CalculateNameAccuracy(property.FormerName, searchWord, _configuration.GetSection(Constants.PropertyFormerName).Value);
            result += CalculateNameAccuracy(property.StreetAddress, searchWord, _configuration.GetSection(Constants.PropertyStreetAddress).Value);
            result += CalculateNameAccuracy(property.State,searchWord,_configuration.GetSection(Constants.PropertyState).Value);
            result += CalculateNameAccuracy(property.City,searchWord,_configuration.GetSection(Constants.PropertyCity).Value);
            return result;
        }

        private float CalculateNameAccuracy(string name, string searchWords, string boostStr)
        {
            if (!float.TryParse(boostStr, out float boost))
                throw new Exception("Parsing boost for calculating name accuracy has failed.");
            float result = 0;

            string[] searchWordArray = searchWords.Split(" ", StringSplitOptions.RemoveEmptyEntries);
            foreach(string searchWord in searchWordArray)
            {
                if (name.ToLower().Contains(searchWord.ToLower()))
                {
                    result += searchWord.Length * boost;
                }
            }
            return result ;
        }
    }
}
