﻿using SmartApartment.Domain.Models;

using System.Collections.Generic;

namespace SmartApartment.Service.ViewModels.Property
{
    public class ListPropertiesViewModel
    {
        public IEnumerable<PropertyDTO> Properties { get; set; }
    }
}
