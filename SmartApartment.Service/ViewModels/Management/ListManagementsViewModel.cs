﻿using SmartApartment.Domain.Models;

using System.Collections.Generic;

namespace SmartApartment.Service.ViewModels.Management
{
    public class ListManagementsViewModel
    {
        public IEnumerable<ManagementDTO> Managements { get; set; }
    }
}
