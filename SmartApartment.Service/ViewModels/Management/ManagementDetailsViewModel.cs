﻿namespace SmartApartment.Service.ViewModels.Management
{
    public class ManagementDetailsViewModel
    {
        public int ManagementID { get; set; }
        public string Name { get; set; }
        public string Market { get; set; }
        public string State { get; set; }
    }
}
