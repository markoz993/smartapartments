﻿using SmartApartment.Domain.Models;

using System.Collections.Generic;

namespace SmartApartment.Service.ViewModels.SearchApi
{
    public class SearchApiDataViewModel
    {
        public IEnumerable<PropertyDTO> Properties { get; set; }
        public IEnumerable<ManagementDTO> Managements { get; set; }

        public string SearchWord { get; set; }
        public string Market { get; set; }

        public int ResultCount { get; set; }
        public int CurrentPage { get; set; }
    }
}
