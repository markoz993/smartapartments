﻿using SmartApartment.Domain.Models;

using System.Collections.Generic;

namespace SmartApartment.Service.ViewModels.Search
{
    public class SearchViewModel
    {
        public IEnumerable<PropertyDTO> Properties { get; set; }
        public IEnumerable<ManagementDTO> Managements { get; set; }

        public string SearchWord { get; set; }
        public string Market { get; set; }
    }
}
