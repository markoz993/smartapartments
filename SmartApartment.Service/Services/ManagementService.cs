﻿using SmartApartment.Repository.Repositories.Abstract;
using SmartApartment.Service.ViewModels.Management;
using SmartApartment.Service.Abstract;
using SmartApartment.Domain.Entities;
using SmartApartment.Domain.Models;

using System.Threading.Tasks;
using System.Linq;

using AutoMapper;

namespace SmartApartment.Service.Services
{
    public class ManagementService : IManagementService
    {
        private readonly IManagementRepository _managementRepo;
        private readonly IMapper _mapper;

        public ManagementService(IManagementRepository managementR, IMapper mapper)
        {
            _managementRepo = managementR;
            _mapper = mapper;
        }

        public async Task<ListManagementsViewModel> GetManagementsList() =>
            new ListManagementsViewModel { Managements = (await _managementRepo.GetAllAsync()).Select(p => _mapper.Map<Management, ManagementDTO>(p)) };

        public async Task<ManagementDetailsViewModel> GetManagementsDetails(int id)
        {
            var model = new ManagementDetailsViewModel { ManagementID = id };
            Management management = await _managementRepo.GetByIDAsync(id);
            if (management != null)
            {
                model.Name = management.Name;
                model.Market = management.Market;
                model.State = management.State;
            }
            return model;
        }
    }
}
