﻿using SmartApartment.Repository.Repositories.Abstract;
using SmartApartment.Service.ViewModels.Property;
using SmartApartment.Service.Abstract;
using SmartApartment.Domain.Entities;
using SmartApartment.Domain.Models;

using System.Threading.Tasks;
using System.Linq;

using AutoMapper;

namespace SmartApartment.Service.Services
{
    public class PropertyService : IPropertyService
    {
        private readonly IPropertyRepository _propertyRepo;
        private readonly IMapper _mapper;

        public PropertyService(IPropertyRepository propertyR, IMapper mapper)
        {
            _propertyRepo = propertyR;
            _mapper = mapper;
        }

        public async Task<ListPropertiesViewModel> GetPropertiesList() =>
            new ListPropertiesViewModel { Properties = (await _propertyRepo.GetAllAsync()).Select(b => _mapper.Map<Property, PropertyDTO>(b)) };

        public async Task<PropertyDetailsViewModel> GetPropertyDetails(int id)
        {
            var model = new PropertyDetailsViewModel { PropertyID = id };
            Property property = await _propertyRepo.GetByIDAsync(id);
            if (property != null)
            {
                model.Name = property.Name;
                model.FormerName = property.FormerName;
                model.StreetAddress = property.StreetAddress;
                model.City = property.City;
                model.State = property.State;
                model.Lat = property.Lat;
                model.Lng = property.Lng;
            }
            return model;
        }
    }
}
