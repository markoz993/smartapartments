﻿using SmartApartment.Repository.Repositories.Abstract;
using SmartApartment.Service.ViewModels.Search;
using SmartApartment.Service.SearchEngine;
using SmartApartment.Service.Abstract;
using SmartApartment.Domain.Entities;
using SmartApartment.Domain.Models;

using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using AutoMapper;

namespace SmartApartment.Service.Services
{
    public class SearchService : ISearchService
    {
        private readonly IPropertyRepository _propertyRepo;
        private readonly IManagementRepository _managementRepo;
        private readonly ISearchEngine _searchEngine;
        private readonly IMapper _mapper;

        public SearchService(
            IPropertyRepository propR,
            IManagementRepository manageR,
            ISearchEngine searchE,
            IMapper mapper)
        {
            _propertyRepo = propR;
            _managementRepo = manageR;
            _searchEngine = searchE;
            _mapper = mapper;
        }

        public async Task<SearchViewModel> GetSearchResults(string searchWord, string market)
        {
            var result = new SearchViewModel { SearchWord = searchWord, Market = market };

            List<PropertyDTO> properties = await GetPropertiesBasedOnMarket(market);
            List<ManagementDTO> managements = await GetManagementsBasedOnMarket(market);

            result.Properties = await _searchEngine.FilterProperties(properties, searchWord);
            result.Managements = await _searchEngine.FilterManagements(managements, searchWord);

            return result;
        }

        private async Task<List<PropertyDTO>> GetPropertiesBasedOnMarket(string market) =>
            !string.IsNullOrWhiteSpace(market) ? 
                (await _propertyRepo.GetPropertiesWithGivenMarket(market))
                    .Select(p => _mapper.Map<Property, PropertyDTO>(p))
                    .ToList() :
                (await _propertyRepo.GetAllAsync())
                    .Select(p => _mapper.Map<Property, PropertyDTO>(p))
                    .ToList();

        private async Task<List<ManagementDTO>> GetManagementsBasedOnMarket(string market) =>
            !string.IsNullOrWhiteSpace(market) ?
                (await _managementRepo.GetManagementsByMarket(market))
                    .Select(m => _mapper.Map<Management, ManagementDTO>(m))
                    .ToList() :
                (await _managementRepo.GetAllAsync())
                    .Select(m => _mapper.Map<Management, ManagementDTO>(m))
                    .ToList();
    }
}
