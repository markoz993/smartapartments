﻿using SmartApartment.Repository.Repositories.Abstract;
using SmartApartment.Service.ViewModels.SearchApi;
using SmartApartment.Service.Abstract;
using SmartApartment.Domain.Models;

using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using AutoMapper;

using Nest;

namespace SmartApartment.Service.Services
{
    public class SearchApiService : ISearchApiService
    {
        private readonly IPropertyRepository _propertyRepo;
        private readonly IManagementRepository _managementRepo;
        private readonly IElasticClient _elasticClient;
        private readonly IMapper _mapper;

        public SearchApiService(
            IPropertyRepository propR, 
            IManagementRepository manageR, 
            IElasticClient elastiClient,
            IMapper mapper)
        {
            _propertyRepo = propR;
            _managementRepo = manageR;
            _elasticClient = elastiClient;
            _mapper = mapper;
        }

        public async Task<SearchApiDataViewModel> GetSearchResults(string searchWord, string market, int currentPage = 1, int resultsPerPage = 10)
        {
            var result = new SearchApiDataViewModel
            {
                SearchWord = searchWord,
                Market = market,
                CurrentPage = currentPage,
                ResultCount = resultsPerPage
            };

            var responseProperty = await _elasticClient.SearchAsync<PropertyDTO>(
                s => s.Query(q => q.QueryString(d => d.Query(searchWord)))
                    .From((currentPage - 1) * resultsPerPage)
                    .Size(resultsPerPage));

            var responseManager = await _elasticClient.SearchAsync<ManagementDTO>(
                s => s.Query(q => q.QueryString(d => d.Query(searchWord)))
                     .From((currentPage - 1) * resultsPerPage)
                     .Size(resultsPerPage));

            result.Properties = FilterProperties(responseProperty, market);
            result.Managements = FilterManagements(responseManager, market);

            return result;
        }

        private IEnumerable<PropertyDTO> FilterProperties(ISearchResponse<PropertyDTO> properties, string market)
        {
            if (string.IsNullOrWhiteSpace(market))
            {
                return properties.Documents;
            }
            return properties.Documents.Where(p => p.Market.ToLower().Contains(market.ToLower()));
        }

        private IEnumerable<ManagementDTO> FilterManagements(ISearchResponse<ManagementDTO> managements, string market)
        {
            if (string.IsNullOrWhiteSpace(market))
            {
                return managements.Documents;
            }
            return managements.Documents.Where(p => p.Market.ToLower().Contains(market.ToLower()));
        }
    }
}
