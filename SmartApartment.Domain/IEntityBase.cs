﻿namespace SmartApartment.Domain
{
    public interface IEntityBase
    {
        int ID { get; set; }
    }
}
