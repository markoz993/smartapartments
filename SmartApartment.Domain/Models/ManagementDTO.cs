﻿namespace SmartApartment.Domain.Models
{
    public class ManagementDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Market { get; set; }
        public string State { get; set; }
    }
}
