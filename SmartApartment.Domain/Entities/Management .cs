﻿namespace SmartApartment.Domain.Entities
{
    public class Management : IEntityBase
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Market { get; set; }
        public string State { get; set; }
    }
}
